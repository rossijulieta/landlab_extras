from setuptools import setup

setup(name='landlab_utils',
      version='0.1',
      description='Landlab extra utilities',
      url='https://gitlab.com/rossijulieta/johnstonebasin/',
      author='Rossi J.',
      author_email='rossijulieta@gmail.com',
      license='MIT',
      packages=['landlab_utils'],
      zip_safe=False)
