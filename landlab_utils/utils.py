import gdal
from scipy.ndimage.filters import uniform_filter
from scipy.ndimage.measurements import variance
from landlab import RasterModelGrid
import numpy as np
from scipy.ndimage.interpolation import zoom

from landlab import RasterModelGrid
from landlab.components.overland_flow import OverlandFlow
from landlab.io import read_esri_ascii # OR from landlab import RasterModelGrid
from landlab.plot import imshow_grid  # plotter functions are optional
from landlab.components import SinkFiller
from landlab.components import SoilInfiltrationGreenAmpt
from landlab.components import FlowAccumulator
from landlab.components import(FlowDirectorD8,
                               FlowDirectorDINF,
                               FlowDirectorMFD,
                               FlowDirectorSteepest)
## Additional Python packages
import os
from landlab.plot.imshow import imshow_grid
from landlab.io import write_esri_ascii
from landlab.plot.drainage_plot import drainage_plot
from landlab.utils import watershed
from landlab.utils.watershed import *
from scipy.ndimage.morphology import binary_dilation
from scipy.ndimage.morphology import generate_binary_structure
import rasterio
from pathlib import Path


def lee_filter(img, size):
    img_mean = uniform_filter(img, (size, size))
    img_sqr_mean = uniform_filter(img**2, (size, size))
    img_variance = img_sqr_mean - img_mean**2

    overall_variance = variance(img)

    img_weights = img_variance / (img_variance + overall_variance)
    img_output = img_mean + img_weights * (img - img_mean)
    return img_output

def save_raster(rmg, layer_list, prefix, template, path='.'):
    '''
    Saves grid to raster file.
    - rmg: RasterModelGrid
    - layer_list: list of layers to be Saved
    - prefix: prefix of the output file name
    - template:
    - path: output folder

    '''

    for layer in layer_list:
        fn = Path(path) / '{}_{}.tif'.format(prefix,layer)
        to_save = rmg.at_node[layer].reshape(rmg.number_of_cell_rows+2, rmg.number_of_cell_columns+2)
        with rasterio.open(dem) as src:
            profile = src.profile
        with rasterio.open(fn, 'w', **profile) as dst:
                dst.write(np.flip(to_save, axis=0).astype(rasterio.float32), 1)
        #print('Saved: {}'.format(fn))


def add_raster_grid(rmg, raster_fn, layer_name):
    '''
    Adds raster_fn to rmg in the layer_name node.
    Example:
    -------
    add_raster_grid(rmg,'ksat.tif', 'hydraulic_conductivity')
    '''
    ds = gdal.Open(raster_fn)
    band = ds.GetRasterBand(1)
    original_array = np.flip(band.ReadAsArray().astype(np.float64), axis=1)
    rmg_original = RasterModelGrid(original_array.shape)
    f_array = np.flip(original_array.flatten(), axis = 0)
    rmg_original.add_field('node', layer_name, f_array)

def create_grid_from_raster(raster_filename):
    '''
    Creates a RasterModelGrid from a gdal supported raster.
    '''
    ds = gdal.Open(raster_filename)
    band = ds.GetRasterBand(1)
    original_array = np.flip(band.ReadAsArray().astype(np.float64), axis=1)
    rmg = RasterModelGrid(original_array.shape)
    z = np.flip(original_array.flatten(), axis = 0)
    rmg.add_field('node', 'topographic__elevation', z)
    return rmg

def apply_lee_filter(original_array, rmg):
        array_lee = lee_filter(original_array, 2)
        z = np.flip(array_lee.flatten(), axis = 0)
        rmg.at_node['topographic__elevation'] = z
        return rmg

def apply_interpolate(rmg):
        elev = rmg.at_node['topographic__elevation'].reshape(array_lee.shape)
        array_interpol = np.flip(np.flip(zoom(elev, 3), axis = 1), axis = 0)
        rmg = RasterModelGrid(array_interpol.shape)
        z = np.flip(array_interpol.flatten(), axis = 0)
        rmg.at_node['topographic__elevation'] = z
        return rmg

def apply_sink_filler(rmg, router, fill_slope):
        sf = SinkFiller(rmg, routing=router,
                        apply_slope=True,
                       fill_slope=fill_slope) # 0.0035 = 0.2
        sf.fill_pits()
        fr = FlowAccumulator(rmg, flow_director=router)  #flow_director='D8'// flow_director='MFD'
        fr.run_one_step()
        return rmg, fr

def apply_basin_mask(rmg, oid):
            mask = watershed.get_watershed_mask(rmg, oid)
            rmg.at_node['watershed_mask'] = mask
            return rmg

## complete preprocess function
def preprocess_grid_from_raster(raster_filename, h_init = 0.0001,
                          d_init = 0.1, oid = 0, fill_slope = 0.0035,
                          dem_null_val = 0, router = 'D8',
                          interpolate=True, basin=True, border=True, lee_filter=False,
                          verbose=True):
    '''

    Example:
    --------

    rmg = preprocess_grid_from_raster('../Img/dem_test.tif')

    '''
    # import DEM 1m
    if verbose:
        print('Creating RasterModelGrid...')
    rmg = create_grid_from_raster(raster_filename)

    #apply lee filter
    if lee_filter == True:
        if verbose:
            print('Applying Lee filter... TODO: FIX NOT WORKING')

        rmg = apply_lee_filter(original_array, rmg)

    #interpol
    if interpolate == True:
        if verbose:
            print('Interpolating...')
        rmg = apply_interpolate(rmg)

        #sink filler
        if verbose:
            print('Filling sinks..')
        rmg, sf = apply_sink_filler(rmg, router, fill_slope)

    else:
        #sinkfiller
        if verbose:
            print('Filling sinks...')

        rmg, sf = apply_sink_filler(rmg, router, fill_slope)


    if verbose:
        print(f'Setting {dem_null_val} as NULL')
    rmg.set_nodata_nodes_to_closed(rmg.at_node['topographic__elevation'], dem_null_val)

    # Estimate the basin map for 0.5 m DEM
    if basin == True:
        if verbose:
            print('Creating basin mask...')
        rmg = apply_basin_mask(rmg, oid)
    else:
        #set no data nodes to inactive boundaries
        if border == True:
            rmg.set_watershed_boundary_condition(z, return_outlet_id=True)


    rmg.add_zeros('node', 'surface_water__depth', dtype=float)    #h
    rmg.add_zeros('node', 'water_surface__slope', dtype=float)
    rmg.add_zeros('node', 'soil_water_infiltration__depth', dtype=float)
    rmg.add_zeros('node', 'rainfall_mask')

    rmg.at_node['surface_water__depth'] += h_init
    rmg.at_node['soil_water_infiltration__depth'] += d_init

    if verbose:
        print(f'RasterModelGrid created with: surface_water__depth, water_surface__slope, soil_water_infiltration__depth, rainfall_mask')
    return rmg

def init_components(rmg, alpha=0.7, mannings_n = 0.035, hydraulic_conductivity_mms = 0.009,
                    soil_bulk_density=1590, initial_soil_moisture_content = 0.01, soil_type='sandy loam'):

    of = OverlandFlow(rmg, steep_slopes = True,
                    #rainfall_intensity = rainfall_ms,
                    g = 9.8,                   # gravity (m/s^2)
                    alpha = alpha,               # time-step factor (nondimensional; from Bates et al., 2010)
                    mannings_n = mannings_n )        # sm−1/3

    #hydraulic_conductivity_mmh = 106
    #hydraulic_conductivity_mms = hydraulic_conductivity_mmh * 3600
    hydraulic_conductivity = hydraulic_conductivity_mms * 0.001   # m/s-1

    ga = SoilInfiltrationGreenAmpt(rmg,
                          #hydraulic_conductivity=rmg.at_node['hydraulic_conductivity'],       # (m/s)
                          hydraulic_conductivity = hydraulic_conductivity,
                          soil_bulk_density=soil_bulk_density,       #float (kg/m**3)
                          rock_density=2650,    # (kg/m**3)
                          #initial_soil_moisture_content = rmg.at_node['soil_moisture_content'],           # (m**3/m**3, 0. to 1.) The fraction of the initial pore space filled with water.
                          initial_soil_moisture_content = initial_soil_moisture_content,           # (m**3/m**3, 0. to 1.) The fraction of the initial pore space filled with water.
                          soil_type=soil_type,                       #A soil type to automatically set soil_pore_size_distribution_index and soil_bubbling_pressure, using mean values from Rawls et al., 1992.
                          volume_fraction_coarse_fragments=0.2,      # (m**3/m**3, 0. to 1.)
                          coarse_sed_flag=False,
                          surface_water_minimum_depth=1.e-8)         # (m) A minimum water depth to stabilize the solutions for surface flood modelling. Leave as the default in most normal use cases.
    return of, ga

def rainfall_mmhr_ms(rainfall_mmhr):
    rainfall_ms = rainfall_mmhr * (2.77778 * 10 ** -7)  #Convert rainfall from mmh−1 to ms−1
    return rainfall_ms
