
Python functions and classes that make common patterns to be used in an application (pre-processinf DEMs, modelling hillslope and catchment hydrological processes) shorter and easier. 
Link to landlab repo https://github.com/landlab/landlab
